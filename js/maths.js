function somme(a,b) {
    var result = new Array();
    var n = a.length;
    for (let i = 0; i < n; i++) {
        result[i] = new Array();
        for (let j = 0; j < n; j++) {
            result[i][j]=a[i][j]+b[i][j];          
        }
    }
    return result;
}

function produit(a,b){
    var result = new Array();
    var n = a.length;
    for (let i = 0; i < n; i++) {
        result[i] = new Array();
        for (let j = 0; j < n; j++) {
            var s = 0;
            for (let k = 0; k < n; k++) {
                s+=a[i][k]*b[k][j];                
            }
            result[i][j]=s;
        }
    }
    return result;
}
