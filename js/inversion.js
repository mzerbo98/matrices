/**
 * Ensemble de fonctions permettant d'effectuer des traitements sur des matrices carrees
 */

//Fonction permettant de copier le contenu d'un vecteur src de taille n dans un autre vecteur dest de taille n
function copie_vecteur(src,dest){
	var n = src.length;
	for(let i=0;i<n;i++)
		dest[i]=src[i];
}

//Fonction permettant de rechercher le pivot a partir d'une ligne k dans une matrice de taille n
function recherche_pivot(k,M){
	var n=M.length;
	for(let i=k;i<n;i++)
		if(M[i][k]!=0)
			return i;
	return -1;
}

//Fonction permettant d'echanger deux lignes k et l dans une matrice M de taille n
function echange(k,l,M){
	var n=M.length;
	var v=new Array(n);
	copie_vecteur(M[k],v);
	copie_vecteur(M[l],M[k]);
	copie_vecteur(v,M[l]);
}

//Fonction permettant de faire la somme des lignes l et p d'une matrice de taille n en multipliant la ligne p par c
function somme_lignes(p,l,c,M){
	var n=M.length;
	for(let i=0;i<n;i++)
		M[l][i]+=c*M[p][i];

}

//Fonction permettant de verifier que les elements de la diagonale d'une matrice sont non nuls
function verification_diagonale(M){
	var n=M.length;
	for(let i=j=0;i<n;i=++j)
		if(M[i][j]==0)
			return false;
	return true;
}

//Fonction permettant de diviser la ligne l d'une matrice de taille n par un reel d
function division_ligne(l,d,M){
	var n=M.length;
	for(let i=0;i<n;i++)
		M[l][i]/=d;
}

//Fonction permettant d'inverser une matrice de taille n et de renvoyer son inverse en utilisant la matrice identité
function inversion(M){
	var n=M.length;
	var inv = new Array(n);
	var i,j;
	//definition de la matrice identite
	for(i=0;i<n;i++){
		inv[i]=new Array(n).fill(0);
		inv[i][i]=1;
	}
	//Boucle permettant de transormer la matrice en matrice triangulaire superieure
	for(i=0;i<n-1;i++){
		var k=recherche_pivot(i,M);
		if(k==-1){//Verification de la validite de l'inddice pivot (-1 signifie aucun pivot)
			return null;
		}else{
			if(k!=i){//Si la ligne de pivot est differente de la ligne de traitement, on les echange
				echange(k,i,M);
				echange(k,i,inv);
			}
			for(j=i+1;j<n;j++){//Boucle permettant d'eliminer les elements de la colonne du pivot
				if(M[j][i]!=0){//On effectue une somme afin d'eliminer l'element concerné
					let coef=-M[j][i]/M[i][i];
					somme_lignes(i,j,coef,M);
					somme_lignes(i,j,coef,inv);
				}
			}
		}
	}
	if(verification_diagonale(M)){//Si un element de la diagonale est nulle, la matrice n'est pas inversible
		for(i=n-1;i>0;i--){//Boucle permettant de rendre la matrice triangulaire inferieure
			for(j=i-1;j>=0;j--){//Elimination des elements dans la colonne de pivot
				if(M[j][i]!=0){
					let coef=-M[j][i]/M[i][i];
					somme_lignes(i,j,coef,M);
					somme_lignes(i,j,coef,inv);
				}
			}
		}//Afin d'obtenir la matrice inverse, on divise chaque ligne de la matrice par l'element de la diagonale afin d'avoir la matrice identité
		for(i=0;i<n;i++)
			division_ligne(i,M[i][i],inv);
		return inv; //la matrice inv sur laquelle on a effectué les mêmes traitements que sur M contient l'inverse de M
	}else{//Si un element de la diagonale est nul, on renvoie null
		return null;
	}
}