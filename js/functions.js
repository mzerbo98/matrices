function elt(id) {
    return document.getElementById(id);
}

function remove(id) {
    var element= elt(id);
    element.parentNode.removeChild(element);
}

function addMatrice(n,id,idconteneur){
    var cont = elt(idconteneur);
    var m = document.createElement('div');
    m.id=id;
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n; j++) {
            var nb = document.createElement('input');
            nb.style="width : 50px;";
            nb.type="number";
            nb.step="any";
            nb.required="true";
            nb.id=id+i+"-"+j;
            m.appendChild(nb);
        }    
        m.appendChild(document.createElement('br'));    
    }
    cont.appendChild(m);
    cont.appendChild(document.createElement('br'));
}

function showMatrice(m,id,idconteneur){
    var cont = elt(idconteneur);
    var mat = document.createElement('table');
    mat.id=id;
    mat.className="table-bordered";
    var n = m.length;
    for (let i = 0; i < n; i++) {
        var ligne = document.createElement('tr');
        for (let j = 0; j < n; j++) {
            var nb = document.createElement('td');
            nb.style="width : 50px;heigth : 50px;";
            nb.innerHTML=m[i][j];
            ligne.appendChild(nb);
        }
        mat.appendChild(ligne);
    }
    cont.appendChild(mat);
}

function getMatrice(id) {
    var tab = elt(id).getElementsByTagName('input');
    var n = Math.floor(Math.sqrt(tab.length));
    var mat = new Array();
    var k=0;
    for (let i = 0; i < n; i++) {
        mat[i] = new Array();
        for (let j = 0; j < n; j++) {
            mat[i][j]=Number(tab[k++].value);
        }        
    }
    return mat;
}
