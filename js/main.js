elt('form_generation').onsubmit=function() {
    var n = document.getElementsByName('taille')[0].value;
    var dest = $("input[name=genre]:checked").val();
    var dest2 = (dest=="matrices"?"matrice":"matrices");
    elt("cont_"+dest).innerHTML="";
    if (dest=="matrices") {
        addMatrice(n,"matriceA","cont_"+dest);
        addMatrice(n,"matriceB","cont_"+dest);
    } else {
        addMatrice(n,"matriceC","cont_"+dest);
    }
    elt("form_"+dest2).style.visibility="hidden";
    elt("cont_"+dest2).innerHTML="";
    elt("form_"+dest).style.visibility="visible";
    return false;
}

elt('form_matrices').onsubmit=function() {
    elt("result").innerHTML="";
    var op = $("input[name=op]:checked").val();
    if (op=="somme") {
        var matA = getMatrice("matriceA");
        var matB = getMatrice("matriceB");
        var res = somme(matA,matB);
        showMatrice(res,"resultat","result");
    } else {
        var matA = getMatrice("matriceA");
        var matB = getMatrice("matriceB");
        var res = produit(matA,matB);
        showMatrice(res,"resultat","result");
    }
    $("#result").draggable();
    return false;
}

elt('form_matrice').onsubmit=function() {
    elt("result").innerHTML="";
    var matC = getMatrice("matriceC");
    //var res = math.inv(matC);
    var res = inversion(matC);
    if(Array.isArray(res)){
        showMatrice(res,"resultat","result");
    }else
        alert("Matrice non inversible");
    return false;
}

document.onkeydown=function(event){
    $("result").stop();
    var speed = 10;
    switch (event.which) {
        case 37:
            $("#result").animate({left :"-=5"},speed);
            break;
        case 38:
            $("#result").animate({top :"-=5"},speed);
            break;    
        case 39:
            $("#result").animate({left :"+=5"},speed);
            break;
        case 40:
            $("#result").animate({top :"+=5"},speed);
            break;
    }
}

if('webkitSpeechRecognition' in window || 'SpeechRecognition' in window){
    alert('La reconnaissance vocale est active');
    var speech = new webkitSpeechRecognition() || new SpeechRecognition();
    speech.continuous = true;
    speech.interimResults = false;
    speech.lang='fr-FR';
    speech.start();
    speech.onresult = function(event){
        res=event.results;
        for (let i = event.resultIndex; i < res.length; i++) {            
            var str = res[i][0].transcript;
            str=str.replace(",",'.');
            console.log(str);            
            if(str.match(/^\s*[-+]{0,1}\s*(\d*\.)?\d+$/)){
                str=str.replace(/\s/g,'');
                document.activeElement.value+=str.trim();
            }
            else if(str.match("1 matrix") || str.match("une matrix") || str.match("1 matrice") || str.match("une matrice") )
                    $("#1mat").prop('checked', true);
            else if(str.match("2 matrix") || str.match("deux matrix") || str.match("2 matrice") || str.match("deux matrice") || str.match(/the matrix/i) || str.match("the matrix"))
                    $("#2mat").prop('checked', true);
            else if(str.match("générer") || str.match("générique"))
                    $("#BtGenerer").trigger("click");
            else if(str.match("calcul") || str.match("calculer") || str.match("calculé")){
                if($("#BtCalculer").css("visibility")=="visible")
                    $("#BtCalculer").focus();
                    $("#BtCalculer").trigger("click");
                }
            else if(str.match("inverser") || str.match("inversé") || str.match("inversion")){
                    if($("#BtInverser").css("visibility")=="visible")
                        $("#BtInverser").focus();
                        $("#BtInverser").trigger("click");
                    }
            else if(str.match("somme"))
                    $("#somme").prop('checked', true);
            else if(str.match("produit"))
                    $("#produit").prop('checked', true);
            else if(str.match("matrix 1") || str.match("matrice 1") || str.match("matrix un") || str.match("matrice un")){
                    if($("#cont_matrices").css("visibility")=="visible")
                        $('#matriceA0-0').focus();
                    else
                        $('#matriceC0-0').focus()
                }
            else if(str.match("matrix 2") || str.match("matrice 2") || str.match("matrix deux") || str.match("matrice deux"))
                    $('#matriceB0-0').focus();
            else if(str.match("matrix") || str.match("matrice")){
                                $('#matriceC0-0').focus();
                            }
            else if(str.match("taille"))
                    $('#taille').focus();
            else if(str.match("suivant"))
                    $(document.activeElement).nextAll("input").first().focus();
            else if(str.match("précédent"))
                    $(document.activeElement).prevAll("input").first().focus();
            else if(str.match("effacer"))
                    $(document.activeElement).val('');
            else if(str.match("un"))
                    document.activeElement.value+=1;
            else 
                    console.log("Non reconnu : "+str);
        }
    }
    speech.onend = function(event){
        console.log("ended");
        speech.start();
    }
}
else
alert('La reconnaissance vocale est inactive');
$('#taille').focus();